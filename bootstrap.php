<?php
/**
 * Wadachi FuelPHP Database Migration Package
 *
 * Tools for writing database schema migrations.
 *
 * @package    wi-migration
 * @version    1.0.0
 * @author     Wadachi, Inc.
 * @link       http://wdch.jp
 */

\Autoloader::add_classes([
  'Wi\\Migration' => __DIR__.'/classes/migration.php',
]);
