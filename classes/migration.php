<?php
/**
 * Wadachi FuelPHP Database Migration Package
 *
 * Tools for writing database schema migrations.
 *
 * @package    wi-migration
 * @version    1.0.0
 * @author     Wadachi, Inc.
 * @link       http://wdch.jp
 */

namespace Wi;

/**
 * データベース·スキーマの移行
 */
class Migration
{
  /**
   * 移行データベース接続を使用してSQL文を実行する
   *
   * @access public
   * @param mixed $statements SQL文かSQL文配列
   */
  function execute_sql($statements)// <editor-fold defaultstate="collapsed" desc="...">
  {
    $class=new \ReflectionClass('\DBUtil');
    $connection = $class->getStaticProperties()['connection']; 
    $statements = is_array($statements) ? $statements : [$statements];

    foreach ($statements as $statement) {
      \DB::query($statement)->execute($connection);
    }
  }// </editor-fold>
}
